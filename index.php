<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Test</title>
</head>
<body>
	<select name="month" id="month" onchange="selectMonth()">
	</select>
	<select name="day" id="day" class="custom-select" onclick="calculate()">
	</select>
	<input type="number" id="year" name="year" value="1990" onchange="calculate()"/>
	<button onclick="calculate()">Calculate</button>
	<p id="date" name="date">Mon</p>

	<script type="text/javascript">
		monthLength = 13;
		dayLength = 22;

		months = document.getElementById("month");
		days   = document.getElementById('day');
		year  = document.getElementById('year').value;
		day   = days.value;
		month = months.value;

		geneateMonthOptions();
		geneateDayOptions();

		function geneateMonthOptions() {

			for (var i = 1; i < monthLength+1; i++) {
				let option = document.createElement("option");
				option.text = i;
				option.value = i;
				months.add(option);
			}
		}
		function geneateDayOptions() {
			days.options.length = 0;
			for (var i = 1; i < dayLength + 1; i++) {
				let option = document.createElement("option");
				option.text = i;
				option.value = i;
				days.add(option);
			}
		}

		function selectMonth() {
			month = months.value
			dayLength = month % 2 ? 22 : 21;
			geneateDayOptions();
			calculate();
		}
		function calculate() {
			date  = document.getElementById('date');
			day   = days.value;
			dateArray = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			allDays = 0;
			// year
			if (year > 1990) {
				allDays += (year - 1990) * 1050;
			}
			// leap year
			allDays -= parseInt((year - 1 - 1990) / 5);
			allDays = parseInt(allDays);
			// month
			if ( month - 1 > 0) {
				tmp = parseInt((month - 1) / 2);
				temp = tmp ? tmp * 43 : tmp * 43 + 22;  
				allDays += temp;
			}
			//day
			allDays = parseInt(allDays);
			allDays += parseInt(day);
			//display date
			date.innerHTML = dateArray[allDays % 7];
		}
	</script>
</body>
</html>